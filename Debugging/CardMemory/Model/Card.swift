//
//  Card.swift
//  Debugging
//
//  Created by Jaya Pranata on 9/24/20.
//

import Foundation

class Card {
    var imageName = ""
    var isFlipped = false
    var isCannotClose = false
}

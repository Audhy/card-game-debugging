//
//  GameScore.swift
//  Debugging
//
//  Created by Jaya Pranata on 9/25/20.
//

import Foundation

class GameScore: Codable {
    var name = ""
    var score: Int = 0
    init(name: String, score: Int ) {
        self.name = name
        self.score = score
    }
}

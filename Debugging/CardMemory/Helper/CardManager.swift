//
//  CardManager.swift
//  Debugging
//
//  Created by Jaya Pranata on 9/24/20.
//

import Foundation

class CardManager {
    func generateCard(numberOfPair: Int) -> [Card] {
        var listCard = [Card]()
        for _ in 1...numberOfPair {
            let randomNumber = Int.random(in: 1...4)
            let cardOne = Card()
            cardOne.imageName = "Artboard \(randomNumber)"
            listCard.append(cardOne)
            let cardTwo = Card()
            cardTwo.imageName = "Artboard \(randomNumber)"
            listCard.append(cardTwo)
        }
        return listCard
    }
}

//
//  GameScoreManager.swift
//  Debugging
//
//  Created by Jaya Pranata on 9/25/20.
//

import Foundation

class GameScoreManager {
    var listScore = [GameScore]()
    init() {
        loadScore()
    }

    func loadScore() {
        if let instalments = UserDefaults.standard.data(forKey: "gameScore") {
            do {
                let temp = try JSONDecoder().decode([GameScore].self, from: instalments)
                listScore = temp
            } catch {
                print("error")
            }
        }
    }

    func saveScore(score: GameScore) {
        listScore.append(score)
        UserDefaults.standard.set(try? JSONEncoder().encode(listScore), forKey: "gameScore")
    }
}

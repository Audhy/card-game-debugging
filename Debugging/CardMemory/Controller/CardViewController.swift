//
//  CardViewController.swift
//  Debugging
//
//  Created by Jaya Pranata on 9/25/20.
//

import UIKit

private let reuseIdentifier = "cell"
class CardViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    var listOfCard: [Card] = []
    var multiplyerScore = 5
    var pointScoreIfMatch = 10
    var firstSelection: IndexPath?
    var score: Int = 0
    var isCheckingCardMatch = false
    var gameScoreManager = GameScoreManager()
    var countCardMatch = 0
    var numberOfPair = 5

    @IBOutlet weak var boardCardCollectionView: UICollectionView!
    @IBOutlet weak var scoreLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        listOfCard = CardManager().generateCard(numberOfPair: numberOfPair)
        // Do any additional setup after loading the view.
    }

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return listOfCard.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as? CardCollectionViewCell {
            setCellImage(cell: cell, indexPath: indexPath)
            return cell
        } else {
            return UICollectionViewCell()
        }
    }

    func setCellImage(cell: CardCollectionViewCell, indexPath: IndexPath) {
        cell.cardBackImageView.image = #imageLiteral(resourceName: "Artboard 5")
        if listOfCard[indexPath.row].isCannotClose || listOfCard[indexPath.row].isFlipped {
            cell.cardBackImageView.image = UIImage(named: listOfCard[indexPath.row].imageName)
        } else {
            cell.cardBackImageView.image = #imageLiteral(resourceName: "Artboard 5")
        }
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let cell = collectionView.cellForItem(at: indexPath) as? CardCollectionViewCell, isCheckingCardMatch == false {
            checkCardSelected(indexPath: indexPath, cell: cell)
        }
    }

    func checkCardSelected(indexPath: IndexPath, cell: CardCollectionViewCell) {
        if listOfCard[indexPath.row].isFlipped == false && listOfCard[indexPath.row].isCannotClose == false {
            cell.openCard(card: listOfCard[indexPath.row])
            if firstSelection != nil {
                checkCardSameOrNot(indexPath: indexPath, cell: cell)
            } else {
                firstSelection = indexPath
            }
        } else {
            listOfCard[indexPath.row].isFlipped = false
            firstSelection = nil
            if listOfCard[indexPath.row].isCannotClose == false {
                cell.closeCard(card: listOfCard[indexPath.row])
            }
        }
    }

    func checkCardSameOrNot(indexPath: IndexPath, cell: CardCollectionViewCell) {
        guard let indexFirstCard = firstSelection?.row else { return }
        isCheckingCardMatch = true
        if listOfCard[indexPath.row].imageName == listOfCard[indexFirstCard].imageName {
            listOfCard[indexPath.row].isCannotClose = true
            listOfCard[indexFirstCard].isCannotClose = true
            firstSelection = nil
            isCheckingCardMatch = false
            calculateScore(isCardMatch: true)
            if countCardMatch == numberOfPair {
                self.inputNameForScore()
            }
        } else {
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1) { [self] in
                calculateScore(isCardMatch: false)
                guard let previousIndexCard = firstSelection else {return}
                guard let cell2 = boardCardCollectionView.cellForItem(at: previousIndexCard) as? CardCollectionViewCell else {return}
                cell.closeCard(card: listOfCard[indexPath.row])
                cell2.closeCard(card: listOfCard[previousIndexCard.row])
                firstSelection = nil
                isCheckingCardMatch = false
            }
        }
    }

    func calculateScore(isCardMatch: Bool) {
        if isCardMatch {
            score += multiplyerScore * pointScoreIfMatch
            countCardMatch += 1
            scoreLabel.text = "\(score)"
            multiplyerScore = 5
        } else {
            multiplyerScore -= 1
            if multiplyerScore < 1 {
                multiplyerScore = 1
            }
        }
    }

    @IBAction func reloadGameClicked(_ sender: UIBarButtonItem) {
        reloadGame()
    }

    func reloadGame() {
        UIView.transition(with: boardCardCollectionView, duration: 2, options: [.transitionCrossDissolve], animations: {
            self.listOfCard.removeAll()
            self.boardCardCollectionView.reloadData()
        }, completion: { _ in
            UIView.transition(with: self.boardCardCollectionView, duration: 2, options: [.transitionCrossDissolve]) {
                self.listOfCard = CardManager().generateCard(numberOfPair: self.numberOfPair)
                self.firstSelection = nil
                self.score = 0
                self.countCardMatch = 0
                self.multiplyerScore = 5
                self.scoreLabel.text = "0"
                self.boardCardCollectionView.reloadData()
            }
        })
    }

    @IBAction func showScoreClicked(_ sender: UIBarButtonItem) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let listScorePage = storyboard.instantiateViewController(identifier: "listScore") as? ListScoreTableViewController {
            listScorePage.listScore = gameScoreManager.listScore
            self.navigationController?.pushViewController(listScorePage, animated: true)
        }
    }

    func inputNameForScore() {
        let alert = UIAlertController(title: "Enter your name", message: "Enter a text", preferredStyle: .alert)
        alert.addTextField { (textField) in
            textField.text = ""
        }
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { [weak alert] (_) in
            let textField = alert?.textFields![0]
            self.gameScoreManager.saveScore(score: GameScore(name: (textField?.text)!, score: self.score))
            self.reloadGame()
        }))
        self.present(alert, animated: true, completion: nil)
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
}

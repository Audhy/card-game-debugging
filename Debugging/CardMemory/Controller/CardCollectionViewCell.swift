//
//  CardCollectionViewCell.swift
//  Debugging
//
//  Created by Jaya Pranata on 9/24/20.
//

import UIKit

class CardCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var cardBackImageView: UIImageView!

    func openCard(card: Card) {
        card.isFlipped = true
        UIView.transition(with: cardBackImageView, duration: 1, options: [.transitionFlipFromLeft]) {
            self.cardBackImageView.image = UIImage(named: card.imageName)
        }
    }

    func closeCard(card: Card) {
        card.isFlipped = false
        UIView.transition(with: cardBackImageView, duration: 1, options: [.transitionFlipFromRight]) {
            self.cardBackImageView.image = #imageLiteral(resourceName: "Artboard 5")
        }
    }
}
